# This function takes:
#  aList: a list of any type of items
#  aDict: a dictionary which maps from the items in aList to other values
# This function should return a list which is the same length as
# aList, where the ith item in the answer list is obtained
# by taking the ith item in aList, looking it up in aDict,
# and using the resulting value.  If that item is not in aDict,
# the corresponding element of the answer list should be None
def maplist(aList, aDict):
    return None #replace this with your answer

# some test cases/examples
assert(maplist([],{1:'a', 3:'c'}) == [])
assert(maplist([1,2,3],{1:'a', 3:'c'}) == ['a', None, 'c'])
assert(maplist(['a','b','c'],{1:'a', 3:'c'}) == [None, None, None])
